package rubberbanding;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.MouseEvent;
import javax.swing.event.MouseInputAdapter;

public class RubberBandingPanel
        extends javax.swing.JPanel {
    // Les variables globales du StateChart
    private Point myFirst, myLast;

    // Les états du StateChart
    enum State {
        IDLE, DRAWING
    };
    
    // La variable d'état
    private State myState = State.IDLE;

    // Les événements à gérer
    // Classe interne anonyme
    MouseInputAdapter eventHandler = new MouseInputAdapter() {
        @Override
        public void mousePressed(MouseEvent e) {
            switch (myState) {
                case IDLE:
                    initLine(e);
                    myState = State.DRAWING;
                    break;
                case DRAWING:
                    break;
            }
        }

        @Override
        public void mouseReleased(MouseEvent e) {
            switch (myState) {
                case IDLE:
                    // impossible
                    break;
                case DRAWING:
                    saveLine();
                    myState = State.IDLE;
                    break;
            }
        }

        @Override
        public void mouseDragged(MouseEvent e) {
            switch (myState) {
                case IDLE:
                    // impossible
                    break;
                case DRAWING:
                    moveLastPoint(e);
                    break;
            }
        }
    };

    public RubberBandingPanel() {
        // On enregistre le listener pour traiter les événements
        addMouseListener(eventHandler);
        addMouseMotionListener(eventHandler);
        setPreferredSize(new Dimension(200, 200));
    }

    private void saveLine() { // Rien pour le moment 
        // TODO : mémoriser les lignes pour pouvoir les redessiner
    }

    // Initialiser le trait
    // Les deux points sont au même endroit
    private void initLine(MouseEvent e) {
        myFirst = e.getPoint();
        myLast = myFirst;
    }

    // Déplacer le dernier point de la ligne
    private void moveLastPoint(MouseEvent e) {
        Graphics g = this.getGraphics();
        g.setXORMode(getBackground());
        // On efface le trait précédent
        g.drawLine(
                myFirst.x,
                myFirst.y,
                myLast.x,
                myLast.y
        );
        
        myLast = e.getPoint();
        // On trace le nouveau trait
        g.drawLine(
                myFirst.x,
                myFirst.y,
                myLast.x,
                myLast.y
        );
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        g.drawString("Repaint me !", 50, 50);
        // TODO : Redessiner les lignes
    }

}
